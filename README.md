# Apigee Paraller Deploy Sample

Pseudo code sample of multimodule Apigee gateway project to test parallel maven build to replicate and debug Apigee Maven Deploy plugin incompatibility with Maven paraller build. 

## Getting started
1. clone project 
2. update properties in profile or create your own progile for your Apigee X.
3. run Maven in paraller build : ´mvn clean install -P destination1 -Dbearer="$(gcloud auth print-access-token)" -T4C´ 
     T=number of threads, for example -T10 runs 10 threds, trailing C=per core for example -T2C runs two threads per CPU core.  
4. Running mvn, you should see warnings: 
```
[WARNING] *****************************************************************
[WARNING] * Your build is requesting parallel execution, but this         *
[WARNING] * project contains the following plugin(s) that have goals not  *
[WARNING] * marked as thread-safe to support parallel execution.          *
[WARNING] * While this /may/ work fine, please look for plugin updates    *
[WARNING] * and/or request plugins be made thread-safe.                   *
[WARNING] * If reporting an issue, report it against the plugin in        *
[WARNING] * question, not against Apache Maven.                           *
[WARNING] *****************************************************************
[WARNING] The following plugins are not marked as thread-safe in proxynn:
[WARNING]   io.apigee.build-tools.enterprise4g:apigee-edge-maven-plugin:2.5.0
[WARNING]
[WARNING] Enable debug to see precisely which goals are not marked as thread-safe.
[WARNING] *****************************************************************
```
5. and it should randomly fail something like (most oftern fail is on sharedflows, which this sample unfortunately does not have any) 
```
[ERROR] Failed to execute goal io.apigee.build-tools.enterprise4g:apigee-edge-maven-plugin:2.5.0:deploy (deploy-bundle) on project shared-request-session: MojoExecutionException: com.google.api.client.http.HttpResponseException: 404 Not Found
[ERROR] {
[ERROR]   "error": {
[ERROR]     "code": 404,
[ERROR]     "message": "generic::not_found: organizations/xxxxxxx/sharedflows/shared-xxx/revisions/244 not found",
[ERROR]     "status": "NOT_FOUND",
[ERROR]     "details": [
[ERROR]       {
[ERROR]         "@type": "type.googleapis.com/google.rpc.RequestInfo",
[ERROR]         "requestId": "11243959239831476427"
[ERROR]       }
[ERROR]     ]
[ERROR]   }
[ERROR] }
```
## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

## License
The Unlicense

## Project status
dead. This is pseudo code. Never tested and never ran (I can not mess up the real enviroments) 